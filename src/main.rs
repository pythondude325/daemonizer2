#[macro_use] extern crate clap;
#[macro_use] extern crate serde;
extern crate serde_yaml;
extern crate nix;

use std::fs::File;
use std::io::{Read, Write};
use std::process::Command;
use std::process::Stdio;

use nix::unistd::Pid;
use nix::sys::signal::{Signal, kill};

#[derive(Deserialize)]
struct Config {
    start: String,
    stop: String,
    control_socket: String,
    io_socket: String,
    pid_file: String
}

// TODO: Ask about spawning proceeses

fn main() {
    let app = clap_app!(daemonizer =>
        (version: "0.2")
        (author: "Graham S. <gischeaffer@gmail.com>")
        (about: "Start shell scripts as daemons and communicate with them using Unix domain sockets.")
        (@arg file: -f --file [FILE] "Path to the daemonizer file (default is daemonizer.yml)")
        (@subcommand start =>
            (about: "Start the daemon")
        )
        (@subcommand stop =>
            (about: "Send stop command to the daemon")
        )
        (@subcommand kill =>
            (about: "Kill the daemon")
            (@group signal =>
                (@arg term: -t --term "Use SIGTERM (default)")
                (@arg int: -i --int "Use SIGINT")
                (@arg kill: -k --kill "Use SIGKILL")
            )
        )
        (@subcommand connect =>
            (about: "Opens a pty to the daemon")
        )
        (@subcommand send =>
            (about: "Send text to daemon")
            (@arg text: +required ... "Text to send to the daemon")
        )
    ).setting(clap::AppSettings::SubcommandRequiredElseHelp);


    let matches = app.get_matches();

    let config_file_path = matches.value_of("file").unwrap_or("daemonizer.yml");
    let config_file = File::open(config_file_path).expect("Failed to open config file");
    let config: Config = serde_yaml::from_reader(config_file).expect("Failed to parse config file.");


    match matches.subcommand() {
        ("start", Some(_sub_m)) => {
            let child = Command::new("socat")
                .arg(format!("EXEC:{}", config.start))
                .arg(format!("UNIX-LISTEN:{},fork", config.io_socket))
                .spawn().expect("Unable to spawn socat");

            let mut pid_file = File::create(config.pid_file).expect("Failed to open PID File");

            write!(&mut pid_file, "{}", child.id()).expect("Failed to write to pid file");
        }

        ("stop", Some(_sub_m)) => {
            println!("Stopping daemon");
            println!("Commands: {:?}", config.stop);
        }

        ("kill", Some(sub_m)) => {
            let mut pid_file = File::open(&config.pid_file).expect("Failed to open PID File");
            let mut pid_string = String::new();
            pid_file.read_to_string(&mut pid_string).expect("Failed to read PID File");

            let pid: Pid = Pid::from_raw(pid_string.parse().expect("Failed to parse PID File"));

            let signal = if sub_m.is_present("term") {
                Signal::SIGTERM
            } else if sub_m.is_present("int") {
                Signal::SIGINT
            } else if sub_m.is_present("kill") {
                Signal::SIGKILL
            } else {
                Signal::SIGTERM
            };

            kill(pid, signal).expect("Failed to kill daemon");

            std::fs::remove_file(&config.pid_file).expect("Failed to remove PID File")
        }

        ("connect", Some(_sub_m)) => {
            let mut child = Command::new("socat")
                .arg("READLINE")
                .arg(format!("UNIX-CONNECT:{}", config.io_socket))
                .spawn().expect("Failed to spawn socat");

            child.wait().expect("Failed to wait for child");
        }

        ("send", Some(sub_m)) => {
            let text = sub_m.values_of("text").unwrap().collect::<Vec<_>>().join(" ");

            println!("Sending text to daemon: \"{}\"", text);

            let mut child = Command::new("socat")
                .arg("-")
                .arg(format!("UNIX-CONNECT:{}", config.io_socket))
                .stdin(Stdio::piped())
                .spawn().expect("Failed to spawn socat");
            
            let stdin = child.stdin.as_mut().expect("Failed to open socat stdin");
            stdin.write_all(text.as_bytes()).expect("Failed to write to socat stdin");
            
            let status = child.wait().expect("Failed to wait for child");

            assert!(status.success(), "Socat failed");
        }

        _ => {
            panic!("Invalid subcommand not caught by clap.")
        }
    }
}
